using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;
using SciptableObject_Gun;

namespace Player_Input
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(PlayerInput))]
    public class NewPlayerInput : MonoBehaviour
    {
        public static NewPlayerInput Instance;

        [Header("Player")]
        private CharacterController controller;
        private PlayerInput playerInput;
        public Vector3 playerVelocity; 
        //private bool groundedPlayer;
        private Transform camTransform;
        
        [SerializeField]
        public float playerSpeed = 2.0f;
        [SerializeField]
        private float jumpHeight = 1.0f;
        [SerializeField]
        private float gravityValue = -9.81f;
        [SerializeField] 
        private float rotationSpeed = 2f;
        public bool canMove = true;

        [Header("Ground Detection")] 
        [SerializeField] private Transform groundCheck;
        [SerializeField] private float groundRadius;
        [SerializeField] private LayerMask whatIsGround;
        [SerializeField]private bool groundedPlayer;

        [Header("Movement")] 
        private InputAction moveAction;
        private InputAction jumpAction;

        [Header("Shooting")]
        private InputAction shootAction;
        private InputAction reloadAction;
        public static Action shootSingle;
        public static Action shootAutomatic;
        public static Action shootShotgun;
        public static Action reloadInput;
        private bool isShooting = false;
        private Gun gun;

        [Header("Ability")]
        private InputAction abitiltyAction;
        private Ability ability;

        [Header("Animation")] 
        [SerializeField] private Animator player_Anim;

        private void Awake()
        {
            Instance = this;
            controller = GetComponent<CharacterController>();
            playerInput = GetComponent<PlayerInput>();
            ability = GetComponent<Ability>();
            player_Anim = GetComponent<Animator>();
            gun = GetComponentInChildren<Gun>();
            camTransform = Camera.main.transform;
            moveAction = playerInput.actions["Move"];
            jumpAction = playerInput.actions["Jump"];
            shootAction = playerInput.actions["Shoot"];
            reloadAction = playerInput.actions["Reload"];
            abitiltyAction = playerInput.actions["Ability"];
        }

        private void OnEnable()
        {
            reloadAction.performed += _ => reloadInput.Invoke();
            shootAction.canceled += _ => isShooting = false;
        }

        private void OnDisable()
        {
            shootAction.canceled -= _ => isShooting = false;
            reloadAction.canceled -= _ => reloadInput.Invoke();
        }
        
        private void OnHit()
        {
            
        }
        void Update()
        {
            Debug.Log(playerSpeed);
            if (canMove)
            {
                //Cursor.lockState = CursorLockMode.Locked;
                //groundedPlayer = controller.isGrounded;
                groundedPlayer = Physics.CheckSphere(groundCheck.position, groundRadius, whatIsGround);
                if (groundedPlayer && playerVelocity.y < 0)
                {
                    playerVelocity.y = 0f;
                }

                Vector2 input = moveAction.ReadValue<Vector2>();
                Vector3 move = new Vector3(input.x, 0, input.y);
                move = move.x * camTransform.right.normalized + move.z * camTransform.forward.normalized;
                move.y = 0f;
                controller.Move(move * Time.deltaTime * playerSpeed);

                if (input.x == 0 && input.y == 0)
                {
                    player_Anim.SetFloat("VelocityX", 0);
                    player_Anim.SetFloat("VelocityZ", 0);
                }
                else if (input.x > 0)
                {
                    player_Anim.SetFloat("VelocityX", 1);
                }
                else if (input.x < 0)
                {
                    player_Anim.SetFloat("VelocityX", -1);
                }
                else if (input.y > 0)
                {
                    player_Anim.SetFloat("VelocityZ", 1);
                }
                else if (input.y < 0)
                {
                    player_Anim.SetFloat("VelocityZ", -1);
                }

                // Changes the height position of the player..
                if (jumpAction.triggered && groundedPlayer)
                {
                    playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
                }

                playerVelocity.y += gravityValue * Time.deltaTime;
                controller.Move(playerVelocity * Time.deltaTime);

                //Rotate toward camera direction
                Quaternion targetRotation = Quaternion.Euler(0, camTransform.eulerAngles.y, 0);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                if (isShooting)
                {
                    shootAutomatic.Invoke();
                }

                gun = GetComponentInChildren<Gun>(); //Please Remove this line when Character is separated, Move to get component before start

                //Please Edit these lines when Character is separated, Move to get component before start
                if (gun.gunData.isAutomatic)
                {
                    shootAction.performed += _ => isShooting = true;
                    Debug.Log("Shoot From Input! Is Automatic");
                }
                if (gun.gunData.isSingle)
                {
                    shootAction.performed += _ => shootSingle.Invoke();
                    Debug.Log("Shoot From Input! Is Single");
                }
                if (gun.gunData.isShotgun)
                {
                    shootAction.performed += _ => shootShotgun.Invoke();
                    Debug.Log("Shoot From Input! Is Is Shotgun");
                }

                if (abitiltyAction.triggered && ability.state == Ability.AbilityStates.ready)
                {
                    ability.ActiveAbility();
                }
            }
            if (!canMove)
            {
                    player_Anim.SetFloat("VelocityX", 0);
                    player_Anim.SetFloat("VelocityZ", 0);
            }
        }

        public void SetCanMoveFalse()
        {
            canMove = false;
        }

        public void IncreaseSpeed(float Amount)
        {
            playerSpeed += Amount;
        }
    }
}