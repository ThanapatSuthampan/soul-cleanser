﻿using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

namespace Script.Object_pool_System.Type
{
    public class Projectile : MonoBehaviour
    {
        public GameObject target;
        private float rotateSpeed = 1.5f;

        public GameObject eBullet;
        public Transform shootPos;
        [Range(5, 20)] public float shootPower;

        private float timeToDestroy = 5f;
        private float attackCooldown = 5f;

        // Start is called before the first frame update
        void Start()
        {
            target = GameObject.FindWithTag("Player");
        }

        private void Update()
        {
            Vector3 direction = target.transform.position - transform.position;
            direction.Normalize();
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotateSpeed * Time.deltaTime);

            var distance = Vector3.Distance(transform.position, target.transform.position);
            shootPower = distance;

            if (distance < 5f)
            {
                shootPower = 5f;
            }
            else if (distance > 20f)
            {
                shootPower = 20f;
            }
            
            //attack
            if (attackCooldown > 0)
            {
                attackCooldown -= Time.deltaTime;
            }
            else
            {
                attackCooldown = 5f;
                
                if (distance <= 20f)
                {
                    StartCoroutine(Fire());
                }
                else
                {
                    StopCoroutine(Fire());
                }
            }
        }
        

        IEnumerator Fire()
        {
            GameObject bullet = Instantiate(eBullet, shootPos.position, Quaternion.identity);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.velocity = shootPos.rotation * Vector3.forward * shootPower;
            
            yield return new WaitForSeconds(timeToDestroy);
            Destroy(bullet);
        }
    }
}