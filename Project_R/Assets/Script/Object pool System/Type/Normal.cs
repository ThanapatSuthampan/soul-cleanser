﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Script.Object_pool_System.Type
{
    public class Normal : MonoBehaviour
    {
        private NavMeshAgent _enemy;
        public GameObject target;
        private BoxCollider hitBox;
        [SerializeField] private float attackCooldown = 2f;

        private void Start()
        {
            hitBox = GetComponent<BoxCollider>();
            _enemy = GetComponent<NavMeshAgent>();
            target = GameObject.FindWithTag("Player");
        }
        
        private void Update()
        {
            _enemy.SetDestination(target.transform.position);
        }
        
        void EnableAttack()
        {
            hitBox.enabled = true;
        }
        void DisableAttack()
        {
            hitBox.enabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponent<PlayerManager>();

            if (player != null)
            {
                player.TakeDamage(10);
                DisableAttack();
                StartCoroutine(CooldownAttack());
            }
        }

        IEnumerator CooldownAttack()
        {
            yield return new WaitForSeconds(attackCooldown);
            EnableAttack();
        }
    }
}