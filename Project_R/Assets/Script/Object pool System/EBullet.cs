﻿using System;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.VFX;

namespace Script.Object_pool_System
{
    public class EBullet : MonoBehaviour
    {
        public VisualEffect explosionVFX;
        [SerializeField] private float radius = 7f;

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                Explode();
            }
        }

        void Explode()
        {
            VisualEffect vfx = Instantiate(explosionVFX, transform.position, Quaternion.identity);
            vfx.Play();
            
            Debug.Log("Explode!");
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider nearbyObject in colliders)
            {
                PlayerManager playerManager = nearbyObject.GetComponent<PlayerManager>();
                if (playerManager != null)
                {
                    playerManager.TakeDamage(15);
                }
                Debug.Log(colliders);
            }
            
            Destroy(vfx.gameObject, 1.2f);
            Destroy(gameObject);
        }
    }
}