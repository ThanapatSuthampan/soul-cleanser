using System.Collections;
using Script.Looting_System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Pool;

namespace Script.Object_pool_System
{
    public class Enemy : MonoBehaviour
    {
        public IObjectPool<Enemy> EnemyPool { get; set; }
    
        private float _timeToFalseActive = 60.0f;
        
        public int current_hp;

        public GameObject floatingTextPrefab;
    
        private int max_hp = 100;

        private EnemyObjectPool pool;

        [SerializeField] private LootBag _lootBag;

        // Start is called before the first frame update
        void Start()
        {
            current_hp = max_hp;
            pool = FindObjectOfType<EnemyObjectPool>();
            _lootBag = FindObjectOfType<LootBag>();
        }


        private void OnEnable()
        {
            StartCoroutine(TimeFalseActive());
        }

        private void OnDisable()
        {
            ResetEnemy();
        }
    
        IEnumerator TimeFalseActive()
        {
            yield return new WaitForSeconds(_timeToFalseActive);
            pool.currentNum--;
            ReturnToPool();
        }

        private void ReturnToPool()
        {
            EnemyPool.Release(this);
            ResetEnemy();
        }
        
        private void ResetEnemy()
        {
            current_hp = max_hp;
        }

        public void TakeDamage(int damageAmount)
        {
            current_hp -= damageAmount;

            if (floatingTextPrefab)
            {
                ShowFloatingText();
            }

            if (current_hp <= 0)
            {
                pool.currentNum--;
                ReturnToPool();
                _lootBag.InstantiateLoot(transform.position);
            }
        }
        
        private void ShowFloatingText()
        {
            var go = Instantiate(floatingTextPrefab, transform.position, Quaternion.identity, transform);
            go.GetComponent<TextMesh>().text = current_hp.ToString();
        }
    }
}
