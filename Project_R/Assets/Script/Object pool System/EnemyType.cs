﻿using UnityEngine;

namespace Script.Object_pool_System
{
    [CreateAssetMenu]
    public class EnemyType : ScriptableObject
    {
        public GameObject enemyPrefab;
        public string enemyName;

        public EnemyType (string enemyName)
        {
            this.enemyName = enemyName;
        }
    }
}