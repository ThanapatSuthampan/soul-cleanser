﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon.StructWrapping;
using Script.Object_pool_System.Type;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.Pool;
using static UnityEngine.GameObject;
using Random = UnityEngine.Random;

namespace Script.Object_pool_System
{
    public class EnemyObjectPool : MonoBehaviour
    {
        private int maxPoolSize = 10;
        private int stackDefaultCapacity = 10;

        [Header("Monster")]
        public List<EnemyType> _enemyType;
        public List<EnemyType> _availibleEnemyType;
        public int currentNum = 0;
        [SerializeField] private int maxNum = 20;
        //public int spawnNum;

        [Header("Wave")]
        [SerializeField] private int currentWave;

        [Header(nameof(Break))]
        private float currentBreakTime;
        [SerializeField] private float breakTime;
        public bool isBreak;
        
        [Header("Text")]
        public TextMeshProUGUI e_Counter;
        public TextMeshProUGUI w_Counter;
        
        /// <summary>
        /// Enemy Pool
        /// </summary>
        private IObjectPool<Enemy> EnemyPool
        {
            get
            {
                if (_enemyPool == null)
                    _enemyPool = new ObjectPool<Enemy>(
                        CreateEnemy,
                        TakeFromPool,
                        ReturnToPool,
                        DestroyPool,
                        true,
                        stackDefaultCapacity,
                        maxPoolSize);

                return _enemyPool;
            }
        }

        private IObjectPool<Enemy> _enemyPool;
        

        private Enemy CreateEnemy()
        {
            EnemyType type = _enemyType[Random.Range(0, _enemyType.Count)];
            GameObject go = Instantiate(type.enemyPrefab, new Vector3(Random.Range(-30f, 30f), 1f, Random.Range(-30f, 30f)), Quaternion.identity);
            go.name = type.enemyName;
            
            switch (go.name)
            {
                case nameof(Normal):
                    var normal = go.GetComponent<Normal>();
                    normal.target = FindGameObjectWithTag("Player");
                    break;
                case nameof(Projectile):
                    var projectile = go.GetComponent<Projectile>();
                    projectile.target = FindGameObjectWithTag("Player");
                    break;
            }

            Enemy enemy = go.GetComponent<Enemy>();
            enemy.EnemyPool = EnemyPool;
            return enemy;
        }

        private void TakeFromPool(Enemy e)
        {
            e.gameObject.SetActive(true);
        }

        private void ReturnToPool(Enemy e)
        {
            e.gameObject.SetActive(false);
            if (currentNum >= maxPoolSize)
            {
                Invoke(nameof(DoSpawn), 0.01f);
            }
            /*Invoke(nameof(DoSpawn), 0.01f);
            if (currentNum < spawnNum)
            {
                CancelInvoke(nameof(DoSpawn));
            }*/
        }

        private void DestroyPool(Enemy e)
        {
            Destroy(e.gameObject);
        }
        
        private void DoSpawn()
        {
            Enemy enemy = EnemyPool.Get();
            enemy.transform.position = new Vector3(Random.Range(-30f, 30f), 1f, Random.Range(-30f, 30f));
        }

        /// <summary>
        /// Main Function
        /// </summary>

        private void Start()
        {
            StartCoroutine(GenerateWave());
            isBreak = false;
        }
        
        private void FixedUpdate()
        {
            if (!isBreak)
            {
                e_Counter.text = string.Format("Spirit Remaining : {0}", currentNum);
                w_Counter.text = $"Wave {currentWave}";
            }

            if (currentNum <= 0)
            {
                currentNum = 0;
                
                currentBreakTime -= Time.deltaTime;
                e_Counter.text = string.Format("Prepare for next wave in {0:0} seconds", currentBreakTime);
                
                if(currentBreakTime <= 0)
                {
                    StartCoroutine(GenerateWave());
                    isBreak = false;
                    GameManager.Instance.UpdateGameState(GameManager.GameState.Play);
                }
            }
        }

        private IEnumerator GenerateWave()
        {
            currentBreakTime = breakTime;
            currentWave += 1;
            currentNum = maxNum * currentWave;
            
            switch (currentWave)
            {
                case 3:
                    _enemyType.Insert(1, _availibleEnemyType[1]);
                    break;
            }

            for (int i = 1; i <= currentNum && i <= maxPoolSize; i++)
            {
                DoSpawn();
            }
            
            yield return new WaitUntil(() => currentNum <= 0);
            isBreak = true;
            GameManager.Instance.UpdateGameState(GameManager.GameState.Break);
        }
    }
}