﻿using System;
using System.Timers;
using UnityEngine;
using UnityEngine.SubsystemsImplementation;

namespace Script
{
    public class ClassSwitcher : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Transform[] weapons;

        [Header("Keys")] 
        [SerializeField] private KeyCode[] keys;

        [Header("Settings")] 
        [SerializeField] private float switchTime;

        [SerializeField] private AbilityData[] abilities;
        private Ability ability;

        private int selectedWeapons;
        private float timeSinceLastSwitch;

        private void Start()
        {
            SetWeapons();
            Select(selectedWeapons);
            GameObject playermc = GameObject.FindGameObjectWithTag("Player");
            ability = playermc.GetComponent<Ability>();
            timeSinceLastSwitch = 0f;
        }
        
        private void SetWeapons()
        {
            weapons = new Transform[transform.childCount];
            
            for (int i = 0; i < transform.childCount; i++)
            {
                weapons[i] = transform.GetChild(i);
            }

            if (keys == null)
            {
                keys = new KeyCode[weapons.Length];
            }
        }

        private void Update()
        {
            int previousSelectedWeapons = selectedWeapons;

            for (int i = 0; i < keys.Length; i++)
            {
                if (Input.GetKeyDown(keys[i]) && timeSinceLastSwitch > switchTime)
                {
                    selectedWeapons = i;
                }
            }

            if (previousSelectedWeapons != selectedWeapons)
            {
                Select(selectedWeapons);
                ability.SetAbilityData(abilities[selectedWeapons]);
            }
            timeSinceLastSwitch += Time.deltaTime;
        }

        private void Select(int weaponIndex)
        {
            for (int i = 0; i < weapons.Length; i++)
            {
                weapons[i].gameObject.SetActive(i == weaponIndex);
            }

            timeSinceLastSwitch = 0f;
        }
    }
}