using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    public UIDocument uiDoc;
    private Button SinglePlayer_Button;
    //private Button Multiplayer_Button;
    //private Button Setting_Button;
    //private Button Credit_Button;
    private Button Exit_Button;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        AssignButton();
        AssignButtonClick();
    }

    void AssignButton()
    {
        SinglePlayer_Button = uiDoc.rootVisualElement.Q<Button>("Single-Button");
        //Multiplayer_Button = uiDoc.rootVisualElement.Q<Button>("Muiti-Button");
        //Setting_Button = uiDoc.rootVisualElement.Q<Button>("Setting-Button");
        //Credit_Button = uiDoc.rootVisualElement.Q<Button>("Credit-Button");
        Exit_Button = uiDoc.rootVisualElement.Q<Button>("Exits-Button");
    }
    void AssignButtonClick()
    {
        SinglePlayer_Button.RegisterCallback<ClickEvent>(StartSinglePlayer);
        //Multiplayer_Button.RegisterCallback<ClickEvent>(StartSinglePlayer);
        //Setting_Button.RegisterCallback<ClickEvent>(StartSinglePlayer);
        //Credit_Button.RegisterCallback<ClickEvent>(StartSinglePlayer);;
        Exit_Button.RegisterCallback<ClickEvent>(Exit);
    }

    private void StartSinglePlayer(ClickEvent clickEV)
    {
        SceneManager.LoadScene(1);
    }
    private void Exit(ClickEvent clickEV)
    {
        Application.Quit();
    }
}
