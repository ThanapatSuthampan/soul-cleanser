using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Player_Input;
using SciptableObject_Gun;

public class UpgradeManager : MonoBehaviour
{
    public static UpgradeManager Instance;

    public List<UpgradeOption> upgradeOptions;
    private List<UpgradeOption> selectedOptions;
    public TextMeshProUGUI[] upgradeOptionNames; 
    public TextMeshProUGUI[] upgradeOptionDesc;

    public bool choosen = false;

    private void Awake()
    {
        Instance = this;
    }

    public void WaveEnded()
    {
        selectedOptions = GetRandomUpgradeOptions(3);
        for (int i = 0; i < selectedOptions.Count; i++)
        {
            upgradeOptionNames[i].text = selectedOptions[i].upgradeName;
            upgradeOptionDesc[i].text = selectedOptions[i].description;
        }
    }
    private List<UpgradeOption> GetRandomUpgradeOptions(int count)
    {
        List<UpgradeOption> randomOptions = new List<UpgradeOption>();
        List<UpgradeOption> availableOptions = new List<UpgradeOption>(upgradeOptions);
        
        for (int i = 0; i < count; i++)
        {
            int randomIndex = Random.Range(0, availableOptions.Count);
            randomOptions.Add(availableOptions[randomIndex]);
            availableOptions.RemoveAt(randomIndex);
        }

        return randomOptions;
    }
    public void ChooseUpgrade(int optionIndex)
    {
        // Handle upgrade selection when the player clicks a button
        UpgradeOption selectedUpgrade = selectedOptions[optionIndex];

        // Apply the selected upgrade to the player's character
        ApplyUpgrade(selectedUpgrade);

        GameManager.Instance.HandleChoosenAbility();
    }
    private void ApplyUpgrade(UpgradeOption upgrade)
    {
        Debug.Log(upgrade.Type);
        switch (upgrade.Type)
        {
            case UpgradeOption.UpgradeType.Health:
                PlayerManager.Instance.IncreaseMaxHealth(upgrade.healthBonus);
                break;
            case UpgradeOption.UpgradeType.Damage:
                SciptableObject_Gun.Gun.Instance.IncreaseDamage(upgrade.damageBonus);
                break;
            case UpgradeOption.UpgradeType.Ability:
                Ability.Instance.DecreaseCooldown(upgrade.cooldownBonus);
                break;
            case UpgradeOption.UpgradeType.Speed:
                Player_Input.NewPlayerInput.Instance.IncreaseSpeed(upgrade.speedBonus);
                break;
        }
    }
}
