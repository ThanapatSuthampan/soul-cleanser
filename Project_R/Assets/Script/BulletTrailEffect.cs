﻿using ExitGames.Client.Photon.StructWrapping;
using UnityEngine;

namespace Script
{
    public class BulletTrailEffect : MonoBehaviour
    {
        [SerializeField] private GameObject bulletTrail;
        
        public void CreateBulletTrail(Transform spawnPoint, Vector3 hitPoint)
        {
            GameObject clone = Instantiate(bulletTrail, spawnPoint.position, Quaternion.identity);
            moveTraits trail = clone.GetComponent<moveTraits>();
            trail.hitPoint = hitPoint;
        }
    }
}