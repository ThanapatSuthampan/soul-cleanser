﻿using System;
using UnityEngine;

namespace Script
{
    public class moveTraits : MonoBehaviour
    {
        public Vector3 hitPoint;
        public float timeToReach;

        private void Start()
        {
            LeanTween.move(this.gameObject, hitPoint, timeToReach);
            Destroy(this.gameObject, timeToReach + 0.01f);
        }
    }
}