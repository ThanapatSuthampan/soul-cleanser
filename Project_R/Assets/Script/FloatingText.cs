using UnityEngine;
using UnityEngine.Serialization;

namespace Script
{
    public class FloatingText : MonoBehaviour
    {
        public float timeToDestroy = 1.0f;
        public Vector3 offset = new Vector3(0, 2, 0);
        public Vector3 randomIntensity = new Vector3(0.5f, 0, 0);
    
        // Start is called before the first frame update
        void Start()
        {
            Destroy(gameObject, timeToDestroy);

            transform.localPosition += offset;
            transform.localPosition += new Vector3(Random.Range(-randomIntensity.x, randomIntensity.x),Random.Range(-randomIntensity.y, randomIntensity.y),
                Random.Range(-randomIntensity.z, randomIntensity.z));

            transform.rotation = Camera.main.transform.rotation;
        }
    }
}
