﻿using System;
using System.Collections.Generic;
using ExitGames.Client.Photon.StructWrapping;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

namespace Script
{
    public class ItemMagnate : MonoBehaviour
    {
        [Range(5,20)]
        public float magnateStrength = 5f;
        [Range(3,10)]
        public float magnateRange = 3f;

        private void FixedUpdate()
        {
            Collider[] hitColliders= Physics.OverlapSphere(transform.position, magnateRange);
            foreach (Collider hitCollider in hitColliders)
            {
                if (hitCollider.CompareTag("Item"))
                {
                    Rigidbody rb = hitCollider.GetComponent<Rigidbody>();
                    Transform trans = hitCollider.GetComponent<Transform>();
                
                    if (rb != null)
                    {
                        //Vector3 towardTarget = trans.position - transform.position;
                        //rb.velocity += towardTarget.normalized * -magnateStrength * Time.fixedDeltaTime;
                        trans.position = Vector3.MoveTowards(trans.position, transform.position, magnateStrength * Time.fixedDeltaTime);
                    }
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, magnateRange);
        }
    }
}