using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public void Retry()
    {
        SceneManager.LoadScene(1);
    }
    public void ReturnToMain()
    {
        SceneManager.LoadScene(0);
    }
}
