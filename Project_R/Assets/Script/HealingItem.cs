using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingItem : MonoBehaviour
{
    private PlayerManager _playerManager;
    [SerializeField] private float radius = 5.0f;
    private Transform center_pos;

    private void Awake()
    {
        center_pos = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] hitColliders = Physics.OverlapSphere(center_pos.position, radius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Player"))
            {
                _playerManager = hitCollider.GetComponent<PlayerManager>();
                Debug.Log("Healing" + hitCollider.name);
                StartCoroutine(HealPlayer());
            }
        }
    }

    IEnumerator HealPlayer()
    {
        yield return new WaitForSeconds(2f);
        _playerManager.HealPlayer(10);
    }
}
