using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Player_Input;
using Cinemachine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    //State
    public GameState state;
    public static event Action<GameState> OnGameStateChange;

    public GameObject Gameover_Screen;
    public GameObject Upgrade_Screen;
    public Player_Input.NewPlayerInput player_input;
    public CinemachineInputProvider camera_input;
    public CinemachineInputProvider camera_aim_input;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        UpdateGameState(GameState.Play);
    }
    public void UpdateGameState(GameState newState)
    {
        state = newState;
        Debug.Log("Game State Changed to: " + newState);
        switch (newState)
        {
            case GameState.Play:
                HandlePlay();
                break;
            case GameState.Died:
                HandleDied();
                break;
            case GameState.Pause:
                HandlePause();
                break;
            case GameState.Unpause:
                HandleUnpause();
                break;
            case GameState.Prestart:
                HandlePreStart();
                break;
            case GameState.Break:
                HandleBreak();
                break;
            case GameState.Win:
                HandleWin();
                break;
            case GameState.Menu:
                HandleMenu();
                break;
            default:
                break;
        }

        OnGameStateChange?.Invoke(newState);
    }

    private void HandleMenu()
    {
        
    }

    private void HandleWin()
    {
        
    }

    private void HandlePreStart()
    {
        
    }
    private void HandleBreak()
    {
        Cursor.lockState = CursorLockMode.Confined;
        UpgradeManager.Instance.WaveEnded();
        Upgrade_Screen.SetActive(true);
        player_input.canMove = false;
        camera_input.gameObject.SetActive(false);
        camera_aim_input.gameObject.SetActive(false);
    }
    public void HandleChoosenAbility()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Upgrade_Screen.SetActive(false);
        player_input.canMove = true;
        camera_input.gameObject.SetActive(true);
        camera_aim_input.gameObject.SetActive(true);
    }
    private void HandleUnpause()
    {
        
    }

    private void HandlePause()
    {
        
    }

    private void HandleDied()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Gameover_Screen.SetActive(true);
        player_input.canMove = false;
        camera_input.gameObject.SetActive(false);
        camera_aim_input.gameObject.SetActive(false);
    }

    private void HandlePlay()
    {
        Cursor.lockState = CursorLockMode.Locked;
        camera_input.gameObject.SetActive(true);
        camera_aim_input.gameObject.SetActive(true);
        player_input.canMove = true;
        Upgrade_Screen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public enum GameState
    {
        Play,
        Died,
        Pause,
        Unpause,
        Prestart,
        Break,
        Win,
        Menu
    }
}
