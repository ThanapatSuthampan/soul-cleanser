using System.Collections;
using UnityEngine;

public class CoroutineManager : MonoBehaviour
{
    private static CoroutineManager instance;

    public static CoroutineManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("CoroutineManager").AddComponent<CoroutineManager>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    // Start a coroutine and return a reference to it
    public Coroutine StartCoroutine(IEnumerator routine)
    {
        return base.StartCoroutine(routine);
    }

    // Stop a coroutine given a reference to it
    public void StopCoroutine(Coroutine routine)
    {
        base.StopCoroutine(routine);
    }
}
