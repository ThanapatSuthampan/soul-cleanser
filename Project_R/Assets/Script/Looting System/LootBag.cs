﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon.StructWrapping;
using Script.Looting_System.Type;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace Script.Looting_System
{
    public class LootBag : MonoBehaviour
    {
        private int maxPoolSize = 100;
        private int stackDefaultCapacity = 10;
        
        public List<LootType> lootList = new List<LootType>();

        private IObjectPool<Loot> LootPool
        {
            get
            {
                if(_lootPool == null)
                    _lootPool = new ObjectPool<Loot>(
                        CreateLoot,
                        TakeFromPool,
                        ReturnToPool,
                        DestroyPool,
                        true,
                        stackDefaultCapacity,
                        maxPoolSize);
                
                return _lootPool;
            }
        }
        
        private IObjectPool<Loot> _lootPool;

        private Loot CreateLoot()
        {
            int randomNumber = Random.Range(1, 101);
            List<LootType> possibleLoot = new List<LootType>();
            
            foreach (LootType item in lootList)
            {
                if (randomNumber <= item.DropChance)
                {
                    possibleLoot.Add(item);
                }
            }
            
            if(possibleLoot.Count > 0)
            {
                LootType droppedItem = possibleLoot[Random.Range(0, possibleLoot.Count)];
                GameObject go = Instantiate(droppedItem.LootPrefab);
                go.name = droppedItem.LootName;
                
                switch (go.name)
                {
                    case nameof(Coin):
                        go.GetComponent<Coin>();
                        break;
                    case nameof(Type.Heal):
                        go.GetComponent<Type.Heal>();
                        break;
                    case nameof(EXP):
                        go.GetComponent<EXP>();
                        break;
                }
                
                Loot loot = go.GetComponent<Loot>();
                loot.LootPool = LootPool;
                return loot;
            }
            
            Debug.Log("No loot dropped");
            return null;
        }
        
        private void TakeFromPool(Loot l)
        {
            l.gameObject.SetActive(true);
        }
        
        private void ReturnToPool(Loot l)
        {
            l.gameObject.SetActive(false);
        }

        private void DestroyPool(Loot l)
        {
            Destroy(l.gameObject);
        }
        
        public void InstantiateLoot(Vector3 spawnPos)
        {
            Loot droppedItem = LootPool.Get();
            droppedItem.transform.position = spawnPos;
        }
    }
}