﻿using UnityEngine;

namespace Script.Looting_System
{
    [CreateAssetMenu]
    public class LootType : ScriptableObject
    {
        public GameObject LootPrefab;
        public string LootName;
        public int DropChance;

        public LootType(string lootName, int dropChance)
        {
            this.LootName = lootName;
            this.DropChance = dropChance;
        }
    }
}