﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Pool;

namespace Script.Looting_System
{
    public class Loot : MonoBehaviour
    {
        public IObjectPool<Loot> LootPool { get; set; }
    
        private float _timeToFalseActive = 15.0f;
        
        private void OnEnable()
        {
            StartCoroutine(TimeFalseActive());
        }

        private void OnDisable()
        {
            ResetLoot();
        }
    
        IEnumerator TimeFalseActive()
        {
            yield return new WaitForSeconds(_timeToFalseActive);
            ReturnToPool();
        }

        private void ReturnToPool()
        {
            LootPool.Release(this);
            ResetLoot();
        }
        
        private void ResetLoot()
        {
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.CompareTag("Player"))
            {
                ReturnToPool();
            }
        }
    }
}