﻿using UnityEngine;

namespace Script.Looting_System.Type
{
    public class EXP : MonoBehaviour
    {
        private int value = 1;
        
        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.CompareTag("Player"))
            {
                PlayerManager mg = other.GetComponent<PlayerManager>();
                mg.IncreaseEXP(value);
                Debug.LogWarning("Exp +" + value);
            }
        }
    }
}