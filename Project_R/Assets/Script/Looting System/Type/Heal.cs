﻿using UnityEngine;

namespace Script.Looting_System.Type
{
    public class Heal : MonoBehaviour
    {
        private int value = 10;
        
        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.CompareTag("Player"))
            {
                PlayerManager mg = other.GetComponent<PlayerManager>();
                mg.HealPlayer(value);
                Debug.LogWarning("Heal +" + value);
            }
        }
    }
}