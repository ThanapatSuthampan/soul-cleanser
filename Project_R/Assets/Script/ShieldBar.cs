using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldBar : MonoBehaviour
{
    [Header("UI")]
    public Slider shield_slider;
    public void SetShieldUI(int Shield)
    {
        shield_slider.value = Shield;
    }
    public void SetMaxShieldhUI(int maxShield)
    {
        shield_slider.maxValue = maxShield;
        shield_slider.value = maxShield;
    }
}

