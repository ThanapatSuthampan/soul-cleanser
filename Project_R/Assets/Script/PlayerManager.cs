using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    [Header("Stat")]
    [SerializeField] private int PlayerHP;
    private int PlayerMaxHP = 100;
    private int PlayerShield;
    [SerializeField] private int PlayerCoin;
    [SerializeField] private int PlayerEXP;
    [Header("References")]
    private Ability ability;

    [Header("UI")]
    private HealthBar healthBar;
    private ShieldBar shieldBar;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayerHP = PlayerMaxHP;
        PlayerCoin = 0;
        PlayerEXP = 0;
        GameObject healthBarObject = GameObject.Find("HealthBar");
        GameObject shieldBarObject = GameObject.Find("ShieldBar");
        healthBar = healthBarObject.GetComponent<HealthBar>();
        shieldBar = shieldBarObject.GetComponent<ShieldBar>();
        healthBar.SetMaxHealthUI(PlayerMaxHP);
        shieldBar.SetMaxShieldhUI(PlayerShield);
        ability = GetComponent<Ability>();
    }
    public void Update()
    {
        PlayerHP = (int)Mathf.Clamp(PlayerHP, 0f, PlayerMaxHP);
        if (ability.abilityData.name.Contains("Shield"))
        {
            ability.abilityData.SetPlayerHP(this.GetComponent<PlayerManager>());
        }

    }
    public void TakeDamage(int damageAmount)
    {
        if (damageAmount < PlayerShield)
        {
            PlayerShield -= damageAmount;
            shieldBar.SetShieldUI(PlayerShield);
        }
        else
        {
            PlayerHP -= damageAmount - PlayerShield;
            PlayerShield = 0;
            shieldBar.SetShieldUI(PlayerShield);
            healthBar.SetHealthUI(PlayerHP);
        }
        if (PlayerHP <= 0)
        {
            PlayerDied();
        }
    }

    private void PlayerDied()
    {
        Debug.Log("PlayerDead!");
        GameManager.Instance.UpdateGameState(GameManager.GameState.Died);
    }

    public void DepleteShield()
    {
        PlayerShield = 0;
        shieldBar.SetShieldUI(PlayerShield);
    }

    public void SetShield(int shieldAmount)
    {
        PlayerShield = shieldAmount;
        shieldBar.SetMaxShieldhUI(PlayerShield);
    }

    public void HealPlayer(int healAmount)
    {
        PlayerHP += healAmount;
        healthBar.SetHealthUI(PlayerHP);
    }

    public void IncreaseMaxHealth(int Amount)
    {
        PlayerMaxHP += Amount;
        PlayerHP += Amount;
        healthBar.SetMaxHealthUI(PlayerMaxHP);
        healthBar.SetHealthUI(PlayerHP);
    }
    
    public void IncreaseCoin(int coinAmount)
    {
        PlayerCoin += coinAmount;
    }
    
    /*public void DecreaseCoin(int coinAmount)
    {
        PlayerCoin -= coinAmount;
    }*/
    
    public void IncreaseEXP(int expAmount)
    {
        PlayerEXP += expAmount;
    }
    
    /*public void DecreaseEXP(int expAmount)
    {
        PlayerEXP -= expAmount;
    }*/
}
