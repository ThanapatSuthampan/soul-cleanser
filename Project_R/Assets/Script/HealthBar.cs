using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [Header("UI")]
    public Slider hp_slider;

    public void SetHealthUI(int health)
    {
        hp_slider.value = health;
    }
    public void SetMaxHealthUI(int maxHealth)
    {
        hp_slider.maxValue = maxHealth;
        hp_slider.value = maxHealth;
    }
}