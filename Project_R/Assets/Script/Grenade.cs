using Script.Object_pool_System;
using UnityEngine;
using UnityEngine.VFX;

namespace Script
{
    public class Grenade : MonoBehaviour
    {
        private float delay = 3f;
        float countdown;
        bool hasExploded = false;
        float radius = 10f;
        
        public VisualEffect explosionVFX;

        // Start is called before the first frame update
        void Start()
        {
            countdown = delay;
        }

        // Update is called once per frame
        void Update()
        {
            countdown -= Time.deltaTime;
            if (countdown < 0f && !hasExploded)
            {
                Explode();
                hasExploded = true;
            }
        }

        void Explode()
        {
            VisualEffect vfx = Instantiate(explosionVFX, transform.position, Quaternion.identity);
            vfx.Play();
            
            Debug.Log("Explode!");
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider nearbyObject in colliders)
            {
                Enemy enemyHealth = nearbyObject.GetComponentInParent<Enemy>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(101);
                }
                Debug.Log(colliders);
            }
            
            Destroy(vfx.gameObject, 1.2f);
            Destroy(gameObject);
        } 
    
    }
}

