using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shield", menuName = "Ability/Shield")]
public class Shield : AbilityData
{
    private Ability ability;
    private PlayerManager _playerManager;
    public float shieldDuration = 5f;
    public float shieldStrength = 50f;

    private Coroutine shieldCoroutine;

    public override void SetAbility(Ability Ability)
    {
        ability = Ability;
    }

    public override void SetPlayerHP(PlayerManager playerManager)
    {
        _playerManager = playerManager;
    }

    public override void Activate(Transform throwPoint)
    {
        ApplyShield();
    }

    void ApplyShield()
    {
        _playerManager.SetShield(50);
        shieldCoroutine = CoroutineManager.Instance.StartCoroutine(RemoveShieldAfterDuration());
    }
    IEnumerator RemoveShieldAfterDuration()
    {
        yield return new WaitForSeconds(shieldDuration);
        _playerManager.DepleteShield();
        Debug.Log("Remove Shield!");
    }
}
