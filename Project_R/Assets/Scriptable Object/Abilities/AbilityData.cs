using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AbilityData : ScriptableObject
{
    [Header("Info")]
    public new string name;

    [Header("Time")]
    public float cooldownTime;
    public float activeTime;

    [Header("Weapon Association")]
    public int associatedWeaponIndex;

    public virtual void Activate(Transform throwPoint) { }
    public virtual void SetAbility(Ability Ability) { }
    public virtual void SetPlayerHP(PlayerManager playerManager) { }

    public void SetAssociatedWeaponIndex(int index)
    {
        associatedWeaponIndex = index;
    }
}
