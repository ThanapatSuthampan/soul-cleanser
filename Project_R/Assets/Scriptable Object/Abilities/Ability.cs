using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Script;
using TMPro;

public class Ability : MonoBehaviour
{
    public static Ability Instance;
    [Header("References")]
    public AbilityData abilityData;

    public float cooldownTime;
    public float activeTime;
    bool activeSkill = false;
    public Transform throwPoint;
    [SerializeField] private TextMeshProUGUI AbilityTMP;
    PlayerManager _playerManager;

    //Handle Ability State
    public enum AbilityStates
    {
        ready,
        active,
        cooldown
    }
    public AbilityStates state = AbilityStates.ready;

    private void Awake()
    {
        Instance = this;
        AbilityTMP.text = "Skill ready! Press Q";
    }

    void Update()
    {
        //Handle State switching
        switch (state)
        {
            case AbilityStates.ready:
                if (activeSkill)
                {
                    abilityData.Activate(throwPoint);
                    activeSkill = false;
                    state = AbilityStates.active;
                    activeTime = abilityData.activeTime;
                }
                break;
            case AbilityStates.active:
                if (activeTime > 0)
                {
                    activeTime -= Time.deltaTime;
                    AbilityTMP.text = "Skill Active!";
                }
                else
                {
                    state = AbilityStates.cooldown;
                    cooldownTime = abilityData.cooldownTime;
                }
                break;
            case AbilityStates.cooldown:
                if (cooldownTime > 0)
                {
                    cooldownTime -= Time.deltaTime;
                    int coolDowntimeInSec = (int)cooldownTime;
                    AbilityTMP.text = coolDowntimeInSec.ToString();
                }
                else
                {
                    state = AbilityStates.ready;
                    AbilityTMP.text = "Skill ready! Press Q";
                }
                break;

        }
        if (abilityData.name.Contains("Shield"))
        {
            abilityData.SetAbility(this.GetComponent<Ability>());
        }
        else if (abilityData.name.Contains("Heal"))
        {
            abilityData.SetAbility(this.GetComponent<Ability>());
        }
        else if (abilityData.name.Contains("Grenade"))
        {
            abilityData.SetAbility(this.GetComponent<Ability>());
        }
    }
    public void ActiveAbility()
    {
        activeSkill = true;
    }

    public void SetAbilityData(AbilityData newData)
    {
        abilityData = newData;
    }
    public void DecreaseCooldown(int Amount)
    {
        abilityData.cooldownTime -= Amount;
    }
}
