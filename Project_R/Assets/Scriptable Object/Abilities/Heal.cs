using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Heal", menuName = "Ability/Heal")]
public class Heal : AbilityData
{
    private Ability ability;
    public GameObject healingPrefab;
    public GameObject playerThrowPoint;
    public float healDuration = 10f;

    private Coroutine healCoroutine;
    // Start is called before the first frame update
    public override void SetAbility(Ability Ability)
    {
        ability = Ability;
    }
    public override void Activate(Transform throwPoint)
    {
        DeployHealing(throwPoint);
    }

    void DeployHealing(Transform throwPoint)
    {
        GameObject healingItem = Instantiate(healingPrefab, throwPoint.position, Quaternion.identity);
        healCoroutine = CoroutineManager.Instance.StartCoroutine(DestroyHeal(healingItem));
    }

    IEnumerator DestroyHeal(GameObject healingItem)
    {
        yield return new WaitForSeconds(5);
        Destroy(healingItem);
    }
}
