using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "GrenadeThrow", menuName = "Ability/GrenadeThrow")]
public class GrenadeThrow : AbilityData
{
    public float throwForce = 100f;
    public GameObject grenadePrefab;
    public GameObject playerThrowPoint;
    private Ability ability;

    private void Awake()
    {

    }
    public override void Activate(Transform throwPoint)
    {
        ThrowGrenade(throwPoint);
    }

    void ThrowGrenade(Transform throwPoint)
    {
        Debug.DrawRay(throwPoint.position, Camera.main.transform.forward * 5f, Color.blue, 2f);
        Vector3 throwDirection = Camera.main.transform.forward;
        GameObject grenade = Instantiate(grenadePrefab, throwPoint.position, Quaternion.identity);
        Rigidbody rb = grenade.GetComponent<Rigidbody>();
        rb.AddForce(throwDirection * throwForce, ForceMode.Impulse);
    }
}
