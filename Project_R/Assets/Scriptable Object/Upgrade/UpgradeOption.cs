using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewUpgradeOption", menuName = "Upgrade Option")]
public class UpgradeOption : ScriptableObject
{
    public string upgradeName;
    public string description;
    public int healthBonus;
    public int damageBonus;
    public float speedBonus;
    public int cooldownBonus;
    public UpgradeType Type;

    public enum UpgradeType
    { 
        Health,
        Damage,
        Ability,
        Speed
    }
}
