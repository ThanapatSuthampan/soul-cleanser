using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName ="Gun", menuName ="Weapon/Gun")]
public class GunData : ScriptableObject
{
    [Header("Info")]
    public new string name;

    [Header("Shooting")]
    public int damage_Head;
    public int damage_Body;
    public float fireRate;
    public float timeBetweenShot; //For single shot gun
    public int bulletPerShot;
    public float maxDistance;
    public bool isAutomatic;
    public bool isSingle;
    public bool isShotgun;

    [Header("Reload")]
    public int currentAmmo;
    public int magSize;
    public float reloadTime;
    public bool isMag; //Reload with magazine
    public bool isSingleshot; //Reload each bullet
    //[HideInInspector]
    public bool reloading = false;
}
