using System.Collections;
using ExitGames.Client.Photon.StructWrapping;
using Script;
using Script.Object_pool_System;
using TMPro;
using UnityEngine;

namespace SciptableObject_Gun
{
    public class Gun : MonoBehaviour
    {
        public static Gun Instance;
        [Header("References")]
        [SerializeField] public GunData gunData;
        
        [SerializeField] private TextMeshProUGUI AmmoTMP;
        [SerializeField] private ParticleSystem m_Muzzle;
        [SerializeField] private Transform muzzle;

        private BulletTrailEffect trailEffect;
        Transform cam;
        private float spread = 30f;
        public Recoil recoil;
        //[SerializeField] private GameObject m_ImpactPrefab;
        //[SerializeField] private Transform m_ImpactPoint;

        private void Awake()
        {
            Instance = this;
            recoil = GetComponent<Recoil>();
            cam = Camera.main.transform;
            if (m_Muzzle == null)
            {
                m_Muzzle.GetComponent<ParticleSystem>();
            }
        }

        private void Start()
        {
            Player_Input.NewPlayerInput.shootAutomatic += ShootAutomatic;
            Player_Input.NewPlayerInput.shootSingle += ShootSingle;
            Player_Input.NewPlayerInput.shootShotgun += ShootShotgun;
            Player_Input.NewPlayerInput.reloadInput += StartReload;
            gunData.reloading = false;
            trailEffect = GetComponent<BulletTrailEffect>();
        }

        private void OnDisable()
        {
            gunData.reloading = false;
        }

        public void StartReload()
        {
            if (!gunData.reloading && this.gameObject.activeSelf)
            {
                Debug.Log("Reloading");
                if (gunData.isMag)
                {
                    StartCoroutine(ReloadMag());
                }
                if (gunData.isSingleshot)
                {
                    StartCoroutine(ReloadSingleBullet());
                }
            }
        }
        private IEnumerator ReloadMag()
        {
            AmmoTMP.color = Color.red;
            gunData.reloading = true;
            yield return new WaitForSeconds(gunData.reloadTime);

            gunData.currentAmmo = gunData.magSize;
            gunData.reloading = false;
            AmmoTMP.color = Color.white;
        }
        private IEnumerator ReloadSingleBullet()
        {
            AmmoTMP.color = Color.red;
            gunData.reloading = true;
            while (gunData.currentAmmo < gunData.magSize)
            {
                yield return new WaitForSeconds(gunData.reloadTime);
                gunData.currentAmmo++;
                AmmoTMP.text = "Ammo : " + gunData.currentAmmo + " / " + gunData.magSize;
                AmmoTMP.color = Color.white;
                yield return new WaitForSeconds(0.2f);
            }
            gunData.reloading = false;
            AmmoTMP.color = Color.white;
        }

        public void StopReload()
        {
            StopAllCoroutines();
            gunData.reloading = false;
        }

        float timeSinceLastShot;
        private bool AutomaticFireRateCal() => !gunData.reloading && timeSinceLastShot > 1f / (gunData.fireRate / 60f); //For Automatic
        private bool SingleFireRateCal => !gunData.reloading && timeSinceLastShot > gunData.timeBetweenShot; //For Single
        public void ShootAutomatic()
        {
            if (gunData.currentAmmo > 0 && !gunData.reloading)
            {
                if (AutomaticFireRateCal())
                {
                    m_Muzzle.Play();
                    Debug.Log("Shoot!");
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    
                    if (Physics.Raycast(ray, out hit, gunData.maxDistance)) //If hit
                    {
                        Debug.Log(hit.transform.name);

                        if (hit.transform != null)
                        {
                            trailEffect.CreateBulletTrail(muzzle, hit.point);
                        }
                        
                        if (hit.collider.CompareTag("EnemyBody"))
                        {
                            Debug.DrawLine(ray.origin, ray.direction * 1000f, Color.green, 3f);
                            Enemy enemyHealth = hit.collider.GetComponentInParent<Enemy>();
                            if (enemyHealth != null)
                            {
                                enemyHealth.TakeDamage(gunData.damage_Body);
                            
                            }
                        }
                        else if (hit.collider.CompareTag("EnemyHead"))
                        {
                            Debug.DrawLine(ray.origin, ray.direction * 1000f, Color.yellow, 3f);
                            Enemy enemyHealth = hit.collider.GetComponentInParent<Enemy>();
                            if (enemyHealth != null)
                            {
                                enemyHealth.TakeDamage(gunData.damage_Head);
                            }
                        }
                    }
                    recoil.GenerateRecoil();
                    gunData.currentAmmo--;
                    timeSinceLastShot = 0;
                    //OnHitObject();
                }
            }
            if (gunData.currentAmmo <= 0)
            {
                StartReload();
            }
        }
        public void ShootSingle()
        {
                if (gunData.currentAmmo > 0 && !gunData.reloading)
                {
                    if (SingleFireRateCal)
                    {
                        m_Muzzle.Play();
                        Debug.Log("Shoot!");
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit, gunData.maxDistance)) //If hit
                            {
                                Debug.Log(hit.transform.name);

                                if (hit.transform != null)
                                {
                                    trailEffect.CreateBulletTrail(muzzle, hit.point);
                                }

                                if (hit.collider.CompareTag("EnemyBody"))
                                {
                                    Debug.DrawLine(ray.origin, ray.direction * 1000f, Color.green, 3f);
                                    Enemy enemyHealth = hit.collider.GetComponentInParent<Enemy>();
                                    if (enemyHealth != null)
                                    {
                                        enemyHealth.TakeDamage(gunData.damage_Body);
                            
                                    }
                                }
                                else if (hit.collider.CompareTag("EnemyHead"))
                                {
                                    Debug.DrawLine(ray.origin, ray.direction * 1000f, Color.yellow, 3f);
                                    Enemy enemyHealth = hit.collider.GetComponentInParent<Enemy>();
                                    if (enemyHealth != null)
                                    {
                                        enemyHealth.TakeDamage(gunData.damage_Head);
                                    }
                                }
                            }
                        recoil.GenerateRecoil();
                        gunData.currentAmmo--;
                        timeSinceLastShot = 0;
                    }
                }
                if (gunData.currentAmmo <= 0)
                {
                    StartReload();
                }
        }
        public void ShootShotgun()
        {
            if (gunData.isShotgun && gunData.reloading)
            {
                StopReload();
            }
            else if (gunData.currentAmmo > 0 && !gunData.reloading && SingleFireRateCal)
            {
                m_Muzzle.Play();
                for (int i = 0; i < gunData.bulletPerShot; i++)
                {
                    Debug.Log("Shoot!");
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    Vector3 spreadDirection = ShotGunSpread();
                    Ray spreadRay = new Ray(ray.origin, spreadDirection);
                    RaycastHit hit;
                    Debug.DrawLine(spreadRay.origin, spreadRay.direction * 1000f, Color.green, 3f);
                    if (Physics.Raycast(spreadRay ,out hit, gunData.maxDistance)) //If hit
                    {
                        Debug.Log(hit.transform.name);
                        if (hit.transform != null)
                        {
                            trailEffect.CreateBulletTrail(muzzle, hit.point);
                        }
                        if (hit.collider.CompareTag("EnemyBody"))
                        {
                            Debug.DrawLine(ray.origin, ray.direction * 1000f, Color.green, 3f);
                            Enemy enemyHealth = hit.collider.GetComponentInParent<Enemy>();
                            if (enemyHealth != null)
                            {
                                enemyHealth.TakeDamage(gunData.damage_Body);
                            
                            }
                        }
                        else if (hit.collider.CompareTag("EnemyHead"))
                        {
                            Debug.DrawLine(ray.origin, ray.direction * 1000f, Color.yellow, 3f);
                            Enemy enemyHealth = hit.collider.GetComponentInParent<Enemy>();
                            if (enemyHealth != null)
                            {
                                enemyHealth.TakeDamage(gunData.damage_Head);
                            }
                        }
                    }
                }
                recoil.GenerateRecoil();
                gunData.currentAmmo--;
                timeSinceLastShot = 0;
            }
            if (gunData.currentAmmo <= 0)
            {
                StartReload();
            }
        }
        private void Update()
        {
            timeSinceLastShot += Time.deltaTime;
            if (gunData.reloading && !gunData.isShotgun)
            {
                AmmoTMP.text = "Reloading...";
            }
            else
            {
                AmmoTMP.text = "Ammo : " + gunData.currentAmmo + " / " + gunData.magSize;
            }
        }

        Vector3 ShotGunSpread()
        {
            Vector3 targetPos = cam.position + cam.forward * gunData.maxDistance;
            targetPos = new Vector3(
                targetPos.x + Random.Range(-spread, spread),
                targetPos.y + Random.Range(-spread, spread),
                targetPos.z + Random.Range(-spread, spread));

            Vector3 direction = targetPos - cam.position;
            return direction.normalized;
        }

        public void IncreaseDamage(int Amount)
        {
            gunData.damage_Body += Amount;
            gunData.damage_Head += Amount;
        }
    }
}

