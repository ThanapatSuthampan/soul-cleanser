using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Recoil : MonoBehaviour
{
    public Cinemachine.CinemachineVirtualCamera player_Camera;
    public Cinemachine.CinemachineVirtualCamera player_Camera2;
    public float verticalRecoilAmplitude = 0.1f;
    public float recoilDuration = 0.1f;

    private CinemachinePOV aimComponent; 
    private CinemachinePOV aimComponent2;
    void Start()
    {
        if (player_Camera != null)
        {
            aimComponent = player_Camera.GetCinemachineComponent<CinemachinePOV>();
            aimComponent2 = player_Camera2.GetCinemachineComponent<CinemachinePOV>();
        }
    }

    public void GenerateRecoil()
    {
        aimComponent.m_VerticalAxis.Value -= verticalRecoilAmplitude;
        aimComponent2.m_VerticalAxis.Value -= verticalRecoilAmplitude;
    }
    void ResetRecoil()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
